TP2 :DNS complet

  

# Table des matières

  

[PARTIE 0: Mise en place  2](https://docs.google.com/document/d/1HT3qACfbZHfm-VCky4WVEzfEXJnAu3jbyO9iiy1jUgA/edit#heading=h.4vja59p3phbc)

[PARTIE 1 : Topologie  3](https://docs.google.com/document/d/1HT3qACfbZHfm-VCky4WVEzfEXJnAu3jbyO9iiy1jUgA/edit#heading=h.34qlmb6gse7n)

[PARTIE 2 : Serveur d’autorité  4](https://docs.google.com/document/d/1HT3qACfbZHfm-VCky4WVEzfEXJnAu3jbyO9iiy1jUgA/edit#heading=h.v81srqlrqqjv)

[PARTIE 3 : Top Level Domain  10](https://docs.google.com/document/d/1HT3qACfbZHfm-VCky4WVEzfEXJnAu3jbyO9iiy1jUgA/edit#heading=h.qb0cbo6nxurz)

[PARTIE 4 : Rootserver  11](https://docs.google.com/document/d/1HT3qACfbZHfm-VCky4WVEzfEXJnAu3jbyO9iiy1jUgA/edit#heading=h.xokc5lxn5f5a)

[TROUBLESHOOTING  13](https://docs.google.com/document/d/1HT3qACfbZHfm-VCky4WVEzfEXJnAu3jbyO9iiy1jUgA/edit#heading=h.p49n54s3agwy)

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

# PARTIE 0: Mise en place

  
  

Sous Virtualbox , on crée tout d’abord un interface de management qui est une interface réseau dédiée aux opérations de configuration et de gestion.

  

Dans /Fichiers/Gestionnaire d’opérations réseau

  

![](https://lh6.googleusercontent.com/r0Xj7fG6fKYqXk_YTNZE2XKCAmpcIHX_8n-wWntnE8OGJbBm5NzRqPPPfqhx2U6vtCfS9o4Bc_zNYm_zJL1gqQBOKl0_gFkpTpnUcfZIsvb3MMetBEEeXqVTzSe0PLF95_xvjLaO)

Avant de commencer, voici les informations concernant la machine sur laquelle j’ai effectué ces manipulations:

  

![](https://lh3.googleusercontent.com/fLB_cEOvONf0-jGRfZCW5rNQ7HtXG8KRKM8LanpkKe2mct_inj7rbW7TlCtWTomZUTUGdhMG3JSBST-y5yPUm-0N61MaPLP8-UzJ9ZCnGmuuuQVIk31JyI5CGlDQn8bqzpgVxHfW)

  

![](https://lh6.googleusercontent.com/ksshcAEX6NOY-l8V1Uz0XcZwB2hZdrhIl3QzUV-twPUnNpycq8byZx2lgb0VQzTKiG-G9g4JCChxEQjfTKlPqFqlc3wD3kLt3cYJlysB-HQAeuVSf_y99GweSS8svd7-BDAEjhQW)

# PARTIE 1 : Topologie

La topologie que j’utilise pour ce TP :

À récupérer sur : https://gitlab.com/M3105/tp/raw/master/files/mydns2.imn

![](https://lh3.googleusercontent.com/KgWGtvOw72jaee8xzvUpMx-yrPmvRkIPNS8RtTnS47NEJ8ioC9s2CwhOXsxhgkn37rie4hlwVirHiC2LrSZ75PvomhfmvUkPiTQgWFQEWinJMKG5oU8tkeswKwBDrnFOmB2Hc6dY)

  
  

# PARTIE 2 : Serveur d’autorité

  
  

Dans cette section , on va configurer le domaine wiki.org sur le serveur dwikiorg de la topologie.

  

On crée tout d’abord un répertoire /etc/named qui contiendra les configurations de zones sur le serveur dwikiorg

  

mkdir -p /etc/named

  
  
  
  

Ensuite on crée et configure le fichier /etc/named/named.conf  qui est le fichier de

configuration principal du serveur.

  

options {  
directory "/etc/named";  
};  
  
zone "." {  
type hint;  
file "named.root";  
};  
  
zone "0.0.127.IN-ADDR.ARPA" {  
type master;  
file "localhost.rev";  
};  
  
zone "wiki.org" {  
type master;  
file "wiki.dir";  
};  
  
zone "7.0.10.IN-ADDR.ARPA" {  
type master;  
file "wiki.rev";  
};

  
  
  

On a créer 4 zones qui sont respectivement : . , 0.0.127.IN-ADDR.ARPA , wiki.org et 7.0.10.IN-ADDR.ARPA.

  

Enfin , on vérifie si la syntaxe du fichier est correct avec la commande :

  

named-checkconf /etc/named.conf

  
  

root@dwikiorg:/etc  #

  

La commande retourne rien , la syntaxe est donc bonne (aucune erreur) .

  

Par la suite , on crée encore 4 fichiers qui se trouvera aussi dans /etc/named :

named.root, localhost.rev, wiki.dir et wiki.rev.

  
  

On édite le fichier /etc/named/named.root qui est notre fichier de configuration de zone directe :

$TTL  60000  
@ IN  SOA dwikiorg.org. root.dwikiorg.org (  
2002102801  ; serial  
28  ; refresh  
14  ; retry  
3600000  ; expire  
0  ; default_ttl  
)  
  
. IN  NS aRootServer.  
aRootServer. A  10.0.0.10  
  

  

On va décortiquer chaque ligne :

  

La première ligne , IN signifie que nous avons une configuration internet.

SOA signifie start of authority ce qui signifie que nous avons un domaine à gérer.

Le nom du serveur suit (ici dwikiorg.org).

Les lignes suivantes contiennent les options du serveur DNS que nous n’allons pas détailler ​[ici](http://www.peerwisdom.org/2013/05/15/dns-understanding-the-soa-record/) .

Ensuite on définit dans ce fichier le nom du serveur pour la zone . ce serveur est aRootServer.

La dernière ligne associe une adresse IP au serveur.

  
  

On édite le fichier localhost.rev :

  

$TTL  86400  
@  IN  SOA  localhost. root.localhost (  
20041128  ; Serial  
28800  ; Refresh  
7200  ; Retry  
3600000  ; Expire  
86400  ; Minimum  
)  
IN  NS  localhost.  
1  IN  PTR  localhost.  
  
  

  

NS signifie NameServer et veut dire que l'information qui suit est le nom d'un serveur dns.

PTR signifie Pointed Record et veut dire qu’il associe un adresse IP à un nom d’hôte.

  

On édite le fichier wiki.dir :

  

$TTL  60000  
@ IN  SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (  
2002102801  ; serial  
28  ; refresh  
14  ; retry  
3600000  ; expire  
0  ; default_ttl  
)  
wiki.org. IN  NS  dwikiorg.wiki.org.  
wiki.org. IN  A  10.0.7.10  
dwikiorg IN  A  10.0.7.10  
www IN  A  10.0.7.11  
gateway IN  A  10.0.7.1

  

A signifie que ce qui suit est une adresse IPv4 (AAAA pour une adresse IPv6).

  
  
  

Enfin , on édite le fichier wiki.rev :

  

$TTL  60000  
@ IN  SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (  
2002102801  ; serial  
28  ; refresh  
14  ; retry  
3600000  ; expire  
0  ; default_ttl  
)  
@ IN  NS  dwikiorg.wiki.org.  
1  IN  PTR  gateway.wiki.org.  
10  IN  PTR  dwikiorg.wiki.org.  
11  IN  PTR  www.wiki.org.

  
  
  

On teste la syntaxe des 4 fichiers qu’on vient de configurer avec la commande

  

named-checkzone <serveur> <chemin du fichier>

  

![](https://lh6.googleusercontent.com/hbUdgloMRRVhX-2FoG6Ih4gnnRs8Cw0GPgMakRlBEKZ7Yd77lBhP4X2n3MxCelxckfWzMjjFqVFLJlSF2OTOIaCrUqWhytp_gDCgjPjtzADBcM-FwpM9tQc7KpnzCwfRn8lLEQaY)

  

![](https://lh4.googleusercontent.com/JaEiElP786n_8sR-N6d0OBL9_PAqoZ6a7SQloWDafZLZLaxh42Ne57JFglQbUFjyjYsmqesLE8iW107ku7zGLJjUoc4R0w-JcrWSYTIVlArol1j994A8ftDOBiJMea4mH6FlAddi)

  
  

![](https://lh5.googleusercontent.com/iPcZdXHonvY4GPwLrRQzYv-xeb9zq6VorqyVV3bXWGP_ycfKjyQxbhK4yqNxhgGTnfHFsuXqFwsHdd52gMnuVFFkgvSU4ujucAxJbriq2qRrGrr1s4rORM7XWn8d0PMgKu2Iv_RL)

  

![](https://lh6.googleusercontent.com/0Cuz_c16kfJr8yIgO7tkpZ5ymlEqBjovgbZDTMM2kV6B_nDDfLU6YCwdx6DyI_hkXyU2WfcUE4vzXv0MD0bTzcstivEQeP8h1vOb3P44UK-dmcYOidc3jT6gUPfaf5ELQ3VhvBkw)

  

La même procédure est à appliquer sur iut.re. ⚠ C'est une zone délégué par iut.re. Donc dans le serveur diutre on doit avoir les lignes spécifiques (en plus des lignes classiques):

Il faudra donc copier les fichiers du dossier wiki.org et les copier dans iut.re qu’il faudra créer aussi et renommer/configurer les fichiers wiki.org et wiki.rev en iut.dir et iut.rev

![](https://lh3.googleusercontent.com/vZzFoRVbCOiHX5b4qm-gq8pdmfVqj-FmOD9jbEYbnnrAvPuFXfn4ar-ioD00J_3deYENWDjnKjcZY-q06r6TuskajbsxNt7S--D4bc_s0cKIvs9L5YweJ0I38F_1SMXR4-5Be_j6)

Ensuite pour éviter de recopier manuellement nos fichiers de configuration dans le serveur diutre on utilise un script deploy.sh avec la commande ./deploy.sh

  

Contenue de notre script  ./deploy.sh pour le domaine wiki.org

  

## configuration de wiki.org  
# configuration du serveur dns  
  
# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg  
himage dwikiorg mkdir -p /etc/named  
  
# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/  
hcp wiki.org/named.conf dwikiorg:/etc/.  
  
# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire /etc/named sur la machine dwikiorg  
hcp wiki.org/* dwikiorg:/etc/named/.  
  
# la commande ci-dessous supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg  
himage dwikiorg rm /etc/named/named.conf

  

Bien évidemment , il faudra ajouter/adapter le script pour les autres domaines.

  
  

Test de la syntaxe des 4 fichiers qu’on a modifié pour le domaine iut.re

  

![](https://lh6.googleusercontent.com/dmJkmGxkYCyIQZziQTcvQ0lyYej9oEeFMceImSMEmkVZ4uR34qhY9-MSbzlulYcK7FnWogPDJzBvRRZleIR3cJ7jGTDNMH0-oqJ-POYpRvL5QitM4-z15dVdBzmIlZoD7L7wY4C-)

  

![](https://lh4.googleusercontent.com/tocZHY9xKlZB5uECFjxpuqlTsYSEuMEdvb7hCMMER3wrw9dvLO7TBfB_xj0zEdStMslLo2NNw8PIlZhirIsMmw3rAPmRKSzX7TJl07bDXcQZWJPRJJsfX41mN63EkOysfm6c9Wr7)

  
  

Test de la syntaxe des 4 fichiers qu’on a modifié pour le domaine rt.iut.re

  

![](https://lh5.googleusercontent.com/jMhab_arIARz_irG486LDpOYTuvs3XUpgnfdq6W_lU64_7K6ib5hXKTep8zlpzzUMz5Qz78ZhmkdddgrMWVZz_-xb3yaVGdtAJICdJYcLzM3nQVZEZmupH_enMK-zeJCz_6rAM7h)

  
  
  
  
  

# PARTIE 3 : Top Level Domain

  
  

Dans cette partie on va configurer les TLD , dans notre cas “.re” et “.org” .

  

Édition du fichier /etc/name.conf

options {  
directory "/etc/named";  
};  
  
zone "." {  
type hint;  
file "named.root";  
};  
  
zone "org" {  
type master;  
file "org";  
};  
  
zone "0.0.127.IN-ADDR.ARPA" {  
type master;  
file "localhost.rev";  
};

  

ici le type hint signifie que l'on donne au serveur DNS une liste de serveur racine.

Test de la syntaxe du fichier /etc/named.conf

![](https://lh3.googleusercontent.com/yJvFnKXpeZUXbobBawNz65RlDGLkYx278KKsHAatfrmnsz3tSJwX719MPDRnymg9G0K9k0sFWGQuaKgOWxdAA0r55fdBxaADdw6UP1hdw9kbde0UHRFjTCH3B3wLxhn8HcK33VMU)

La commande renvoie rien , la syntaxe est donc correcte.

Édition du fichier /etc/named/org

  

$TTL  60000  
@ IN  SOA dorg.org. root.dorg.org (  
2002102801  ; serial  
28  ; refresh  
14  ; retry  
3600000  ; expire  
0  ; default_ttl  
)  
  
@  IN  NS dorg.org.  
dorg.org.  IN  A  10.0.2.10  
  
wiki.org.  IN  NS dwikiorg.wiki.org.  
dwikiorg.wiki.org.  IN  A  10.0.7.10

  

Les 2 dernières lignes du fichier indique qui gère le domaine wiki.org (nom du serveur et adresse ip)

  

# PARTIE 4 : Rootserver

  

Ici on va configurer le serveur racine aRootServer.

Edition du fichier /etc/named.conf

On crée 3 zones : . , 0.0.127.IN-ADDR.ARPA et IN-ADDR.ARPA

  

options {  
directory "/etc/named";  
};  
  
zone "." {  
type master;  
file "root";  
};  
  
zone "0.0.127.IN-ADDR.ARPA" {  
type master;  
file "localhost.rev";  
};  
  
zone "IN-ADDR.ARPA" {  
type master;  
file "in-addr.arpa";  
};

  
  

Test du fichier  /etc/named.conf

  

Édition du fichier  /etc/named/root

Les quatre dernière ligne de ce fichier référence les domaine .re et .org.

  

$TTL  60000  
@ IN  SOA aRootServer. root.aRootServer (  
2002102801  ; serial  
28800  ; refresh  
14400  ; retry  
3600000  ; expire  
0  ; default_ttl  
)  
  
@ IN  NS aRootServer.  
aRootServer. IN  A  10.0.0.10  
  
org. IN  NS dorg.org.  
dorg.org. IN  A  10.0.2.10  
  
re. IN  NS dre.re.  
dre.re. IN  A  10.0.3.10

  
  
  
  
  
  
  
  
  
  
  
  
  

# TROUBLESHOOTING
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk0MjE0MTQ4Ml19
-->